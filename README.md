Scripts used to produce the results in the manuscript  Zoghlami, A., Refahi, Y.,  Terryn, C., Paës, G., (2020) [Three-Dimensional Imaging of Plant Cell Wall Deconstruction Using Fluorescence Confocal Microscopy](https://www.mdpi.com/2673-4079/1/2/7/htm), Sustainable Chemistry, 1(2), 75-84. 

Information about how to install the dependencies and the scripts can be found in [the wiki page.](https://gitlab.com/farelab/teamyr/publications/zoghlami_et_al_sus_chem_2020/-/wikis/home)


## contact

Yassin Refahi (yassin.refahi@inrae.fr)
