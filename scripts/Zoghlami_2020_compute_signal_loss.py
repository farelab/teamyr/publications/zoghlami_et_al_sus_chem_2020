############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# Script to segment confocal images as described in manuscript: 
# Zoghlami, A., Refahi, Y.,  Terryn, C., Paes, G., Three-Dimensional Imaging of Plant Cell Wall Deconstruction Using Fluorescence Confocal Microscopy, Sus. Chem. 2020.
#
############################################################################

import numpy as np
from Zoghlami_2020_parameters import savePath, t24h_intensity_file_name
from biomodlab.tiff.bmtiff import bmimread


# registered image name
t0h_on_t24h_file_name = savePath + "/t0_NL_registered.tif"



im24, info24 = bmimread(t24h_intensity_file_name)
im24 = np.swapaxes(im24, 2, 0)

im0_on_im24, info0_reg = bmimread(t0h_on_t24h_file_name)
im0_on_im24 = np.swapaxes(im0_on_im24, 2, 0)


# define the zone for which you would like to compute the signal loss

X_first_last = [10, 240]
Y_first_last = [10, 240]
Z_first_last = [10, 150]



sum_0h_on_24h = np.sum(im0_on_im24[X_first_last[0]:X_first_last[1], Y_first_last[0]:Y_first_last[1], Z_first_last[0]:Z_first_last[1]])

sum_24h = np.sum(im24[X_first_last[0]:X_first_last[1], Y_first_last[0]:Y_first_last[1], Z_first_last[0]:Z_first_last[1]])


print "signal loss between two images is = %0.2f %%"%(sum_24h/float(sum_0h_on_24h)*100)
