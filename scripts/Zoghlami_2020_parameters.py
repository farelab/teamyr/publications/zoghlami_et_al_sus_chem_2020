############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# Script to segment confocal images as described in manuscript: 
# Zoghlami, A., Refahi, Y.,  Terryn, C., Paes, G., Three-Dimensional Imaging of Plant Cell Wall Deconstruction Using Fluorescence Confocal Microscopy, Sus. Chem. 2020.
#
############################################################################


import os

# confocal image name before hydrolysis
t0h_intensity_file_name = "t0h.tif"

# confocal image name before hydrolysis
t24h_intensity_file_name = "t24h.tif" 

                
# path to save the registration files
mainPath = ""


path0 = t0h_intensity_file_name
path1 = t24h_intensity_file_name

# path to Blochmathing algorithms
pathApplyTrsf = "applyTrsf"
pathBlockmatching = "blockmatching"


affineReg = True  # set to TRUE if a affine registration is needed
nonlinearReg = True # set to TRUE if a nonlinear registration is needed


paramALT = 4  
elastic_sigma = paramALT
fluid_sigma = paramALT

savePath = mainPath + "registration/"

try:
    os.mkdir(savePath)
except OSError:
    print "folder exists"


try:
    os.mkdir(mainPath + "segmentations")
except OSError:
    print "folder exists"

