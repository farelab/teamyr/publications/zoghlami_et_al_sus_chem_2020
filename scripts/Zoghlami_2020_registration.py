############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# Script to segment confocal images as described in manuscript: 
# Zoghlami, A., Refahi, Y.,  Terryn, C., Paes, G., Three-Dimensional Imaging of Plant Cell Wall Deconstruction Using Fluorescence Confocal Microscopy, Sus. Chem. 2020.
#
############################################################################


import numpy as np
import sys
import os
from openalea.image.serial.basics import imread, imsave
from biomodlab.tiff.bmtiff import (getVoxelDimensions, bmimread, 
                                   bmimsave, bmimsaveRGB)

from Zoghlami_2020_parameters import (pathApplyTrsf, pathBlockmatching, 
                                     paramALT, elastic_sigma, fluid_sigma, 
                                     savePath, path0, path1, affineReg, 
                                     nonlinearReg)

t0=imread(path0)
t1=imread(path1)


res0 = getVoxelDimensions(path0)
res1 = getVoxelDimensions(path1)


t0.resolution = res0[0], res0[1], res0[2] 
t1.resolution = res1[0], res1[1], res1[2] 



if affineReg:
    imsave('%st0.inr'%savePath, t0)
    imsave('%st1.inr'%savePath, t1)
    
    newImage = np.swapaxes(t0, 0, 2)
    bmimsave(savePath + "/t0.tif", newImage, voxelWidth = res0[0], voxelHeight = res0[1], voxelDepth = res0[2], type = "intensity")
    
    newImage = np.swapaxes(t1, 0, 2)
    bmimsave(savePath + "/t1.tif", newImage, voxelWidth = res1[0], voxelHeight = res1[1], voxelDepth = res1[2], type = "intensity")
    
    pyramid_lowest_level = 2   
    os.system(pathBlockmatching +
              " -ref %st1.inr"%savePath +
              " -flo %st0.inr"%savePath +
              " -res %st0_affine_registered.inr"%savePath +
              " -res-trsf %saffine_full_trsf"%savePath +
              " -trsf-type affine" +
              " -estimator wlts" +
              " -pyramid-highest-level 5" +
              " -pyramid-lowest-level %d"%pyramid_lowest_level +
              " -lts-fraction 0.55")
    
    image_affine_reg = imread(savePath + "/t0_affine_registered.inr")
    newImage = np.swapaxes(image_affine_reg, 0, 2)
    bmimsave(savePath + "/t0_affine_registered.tif", newImage, voxelWidth = res1[0], voxelHeight = res1[1], voxelDepth = res1[2], type = "intensity")

t1, info0 = bmimread("%st1.tif"%savePath)
imAffineReg, info1 = bmimread("%st0_affine_registered.tif"%savePath)



newIm = np.empty(shape = (t1.shape[0], t1.shape[1], t1.shape[2], 3), dtype = t1.dtype)
newIm[..., 0] = t1
newIm[..., 1] = imAffineReg
newIm[..., 2] = 0
fName = "%sRGB_affine.tif"%savePath
bmimsaveRGB(fName, newIm, voxelWidth = info1["voxelWidth"], voxelHeight = info1["voxelHeight"], voxelDepth = info1["voxelDepth"], type = "intensity")
 
if nonlinearReg:
    os.system(pathBlockmatching +
              " -ref %st1.inr"%savePath +
              " -flo %st0.inr"%savePath +
              " -init-trsf %saffine_full_trsf"%savePath +
              " -res %st0_NL_registered.inr"%savePath +
              " -res-trsf %sNL_trsf.inr"%savePath +
              " -trsf-type vectorfield" +
              " -estimator wlts" +
              " -py-gf" +
              " -pyramid-highest-level 5" +
              " -pyramid-lowest-level %d"%pyramid_lowest_level +
              " -elastic-sigma %f %f %f"%(elastic_sigma, elastic_sigma, elastic_sigma) +
              " -fluid-sigma %f %f %f"%(fluid_sigma, fluid_sigma, fluid_sigma))
    
    image_affine_reg = imread(savePath + "/t0_NL_registered.inr")
    newImage = np.swapaxes(image_affine_reg, 0, 2)
    bmimsaveRGB(savePath + "/t0_NL_registered.tif", newImage, voxelWidth = res1[0], voxelHeight = res1[1], voxelDepth = res1[2], type = "intensity")


imNLReg, info1 = bmimread("%st0_NL_registered.tif"%savePath)
newIm = np.empty(shape = (t1.shape[0], t1.shape[1], t1.shape[2], 3), dtype = t1.dtype)
newIm[..., 0] = t1
newIm[..., 1] = imNLReg
newIm[..., 2] = 0
fName = "%sRGB_NL.tif"%savePath
bmimsaveRGB(fName, newIm, voxelWidth = info1["voxelWidth"], voxelHeight = info1["voxelHeight"], voxelDepth = info1["voxelDepth"], type = "intensity")



