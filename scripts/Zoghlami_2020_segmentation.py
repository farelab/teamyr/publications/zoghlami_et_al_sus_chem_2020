############################################################################
#
# File author(s): Yassin REFAHI <yassin.refahi@inrae.fr>
# Copyright (c) 2017 - 2020, Yassin Refahi
# Script to segment confocal images as described in manuscript: 
# Zoghlami, A., Refahi, Y.,  Terryn, C., Paes, G., Three-Dimensional Imaging of Plant Cell Wall Deconstruction Using Fluorescence Confocal Microscopy, Sus. Chem. 2020.
#
############################################################################

import numpy as np
from skimage.filter import threshold_otsu


from biomodlab.tiff.bmtiff import bmimread, bmimsave
# You can replace the tiff reader and writer with methods from libtiff and tifffile packages

from Zoghlami_2020_parameters import savePath, mainPath, t0h_intensity_file_name, t24h_intensity_file_name



# registered image name
t0h_on_t24h_file_name = savePath + "/t0_NL_registered.tif"

# segmented images' files directory
segmented_images_dir = mainPath + "segmentations/"


im0, info0 = bmimread(t0h_intensity_file_name)

im24, info24 = bmimread(t24h_intensity_file_name)

im0_on_im24, info0_reg = bmimread(t0h_on_t24h_file_name)

threshold = threshold_otsu(im0)


backgorundLabel = 0

cellWallsLabel = 1
# to visualize with BIOMODLAB set cellWallsLabel to 125

mask0 = im0 < threshold
mask24 = im24 < threshold

im0[mask0] = 0
im0[np.bitwise_not(mask0)] = cellWallsLabel

im24[mask24] = 0
im24[np.bitwise_not(mask24)] = cellWallsLabel


mask_im0_on_im24 = im0_on_im24 < threshold
im0_on_im24[mask_im0_on_im24] = 0
im0_on_im24[np.bitwise_not(mask_im0_on_im24)] = cellWallsLabel

totalSum_t0 = np.sum((im0 == cellWallsLabel).flatten())
totalSum_t24 = np.sum((im24 == cellWallsLabel).flatten())


print "%0.2f %% of the tissue before hydrolysis is remaining. The computation is done without registration."%(float(totalSum_t24)/float(totalSum_t0)*100)


bmimsave(segmented_images_dir + "/t0_segmented_%d.tif"%threshold, im0 , voxelWidth = info0["voxelWidth"], voxelHeight = info0["voxelHeight"], voxelDepth = info0["voxelDepth"], type = "segmented")

bmimsave(segmented_images_dir + "/t24_segmented_%d.tif"%threshold, im24 , voxelWidth = info24["voxelWidth"], voxelHeight = info24["voxelHeight"], voxelDepth = info24["voxelDepth"], type = "segmented")

bmimsave(segmented_images_dir + "/t0_NL_registred_segmented_%d.tif"%threshold, im0_on_im24 , voxelWidth = info0_reg["voxelWidth"], voxelHeight = info0_reg["voxelHeight"], voxelDepth = info0_reg["voxelDepth"], type = "segmented")



subtractedImage = np.subtract(im0_on_im24, im24)

bmimsave(segmented_images_dir + "/t0_NL_subtracted_t24_segmented_%d.tif"%threshold, subtractedImage , voxelWidth = info0_reg["voxelWidth"], voxelHeight = info0_reg["voxelHeight"], voxelDepth = info0_reg["voxelDepth"], type = "segmented")
# new cell wall label at 24h due to the movement of the sample in other words the part of the image which is in 24h image but not in in 0h image  
newCellWallLabelAt24h = 256 - cellWallsLabel




binc = np.bincount(subtractedImage.flatten())

backgroundVoxelNumber, degradedVoxelNumber, newTissueVoxelNumberAt24h = binc[0], binc[cellWallsLabel], binc[newCellWallLabelAt24h]

totalSum_t0_on_t24 = np.sum(im0_on_im24 == cellWallsLabel)

print "%0.2f %% of the tissue before hydrolysis is remaining. The computation is done after registration, therefore more precise."%((1 - float(degradedVoxelNumber)/totalSum_t0_on_t24)*100)

print "%0.2f %% of the tissue before hydrolysis is degraded. The computation is done after registration."%((float(degradedVoxelNumber)/totalSum_t0_on_t24)*100)



# generating a combined image of segmented registered image at 0h and segmented image at 24h, Figure 9 

combined_image_new_tissueAt24h_label = 2
combined_image_remaining_tissue_label = 125
combined_image_degraded_tissue_label = 255
 

# combinedImage = np.array(im0_on_im24)
# 
# combinedImage[combinedImage == cellWallsLabel] = combined_image_remaining_tissue_label
# 
# subtractedImage[subtractedImage == cellWallsLabel] = combined_image_degraded_tissue_label
# subtractedImage[subtractedImage == newCellWallLabelAt24h] = combined_image_new_tissueAt24h_label


combinedImage = np.array(im0_on_im24)

combinedImage[combinedImage == cellWallsLabel] = combined_image_remaining_tissue_label

combinedImage[subtractedImage == cellWallsLabel] = combined_image_degraded_tissue_label
combinedImage[subtractedImage == newCellWallLabelAt24h] = combined_image_new_tissueAt24h_label



# combinedImage[subtractedImage != 0] = subtractedImage[subtractedImage != 0]


bmimsave(segmented_images_dir + "/t0_NL_combined_t24_segmented_%d.tif"%threshold, combinedImage , voxelWidth = info0_reg["voxelWidth"], voxelHeight = info0_reg["voxelHeight"], voxelDepth = info0_reg["voxelDepth"], type = "segmented")

degraded_voxel_number_on_combined_image = np.sum(combinedImage == combined_image_degraded_tissue_label)
remaining_voxel_number_on_combined_image = np.sum(combinedImage == combined_image_remaining_tissue_label)
new_tissueat24h_voxel_number_on_combined_image = np.sum(combinedImage == combined_image_new_tissueAt24h_label)







